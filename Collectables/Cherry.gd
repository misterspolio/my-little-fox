extends Area2D

var berryIsCollected = false

func _on_body_entered(body):
	if body.name == "Player" and not berryIsCollected:
		berryIsCollected = true
		Game.cherries += 1
		Utils.saveGame()
		if Game.cherries >= Game.maxCherries:
			get_tree().change_scene_to_file("res://end.tscn")
		
		var tween = get_tree().create_tween()
		var tween1 = get_tree().create_tween()
		
		tween.tween_property(self, "position", position - Vector2(0, 35), 0.35)
		tween1.tween_property(self, "modulate:a", 0, 0.3)
		
		tween.tween_callback(queue_free)
