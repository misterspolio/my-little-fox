extends CharacterBody2D

const SPEED = 200.0
const RUNNING_SPEED = 300.0
const JUMP_VELOCITY = -400.0
const WALL_JUMP_VELOCITY = 500.0
var canDoubleJump = false

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")

@onready var anim = get_node("AnimationPlayer")
@onready var leftArea = get_node("LeftDetection")
@onready var rightArea = get_node("RightDetection")
	
func _physics_process(delta):
	handleGravity(delta)

	handleMove()

	move_and_slide()
	
	handleDeath()
	
func handleGravity(delta):
	velocity.y += gravity * delta
	if is_on_floor():
		velocity.y = 0

func handleMove():
	handleDirection()
	handleJump()
	
func handleJump():
	# Classic jump
	if is_on_floor():
		canDoubleJump = true
		if Input.is_action_just_pressed("ui_accept"):
			jump()
	# Wall jump or Double Jump
	elif (canDoubleJump or is_on_wall()) and Input.is_action_just_pressed("ui_accept"):
		canDoubleJump = false
		jump()

func jump():
	velocity.y = JUMP_VELOCITY
	anim.play("Jump")

func handleDirection():
	# Get the input direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	var direction = Input.get_axis("ui_left", "ui_right")
	
	if direction == -1:
		get_node("AnimatedSprite2D").flip_h = true
	elif direction == 1:
		get_node("AnimatedSprite2D").flip_h = false
	if direction:
		velocity.x = direction * getSpeed()
		if velocity.y == 0:
			anim.play("Run")
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED)
		if velocity.y == 0:
			anim.play("Idle")

	if velocity.y > 0:
			anim.play("Fall")

func getSpeed() -> float:
	var speed = SPEED
	
	if Input.is_action_pressed("ui_up"):
		speed = RUNNING_SPEED
	
	return speed

func handleDeath():
	if isPlayerDead():
		killPlayer()

func isPlayerDead() -> bool :
	return Game.playerHp <= 0;

func killPlayer():
	self.queue_free()
	get_tree().change_scene_to_file("res://end.tscn")
